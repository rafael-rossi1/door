package br.com.itau.door.controllers;

import br.com.itau.door.models.Door;
import br.com.itau.door.services.DoorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/door")
public class DoorController {

    @Autowired
    DoorService doorService;

    @PostMapping
    public Door create(@RequestBody @Valid Door door) {
        return doorService.create(door);
    }

    @GetMapping("/{id}")
    public Door getById(@PathVariable Long id) {
        Door door = doorService.getById(id);
        return door;
    }

}
