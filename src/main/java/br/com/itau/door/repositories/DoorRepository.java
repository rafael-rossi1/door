package br.com.itau.door.repositories;

import br.com.itau.door.models.Door;
import org.springframework.data.repository.CrudRepository;

public interface DoorRepository extends CrudRepository <Door,Long>{
}
