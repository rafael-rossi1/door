package br.com.itau.door.services;

import br.com.itau.door.exception.DoorNotFoundException;
import br.com.itau.door.models.Door;
import br.com.itau.door.repositories.DoorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DoorService {


    @Autowired
    private DoorRepository doorRepository;

    public Door create(Door door) {
        return doorRepository.save(door);
    }

    public Door getById(Long id) {
        Optional<Door> byId = doorRepository.findById(id);

        if(!byId.isPresent()) {
            throw new DoorNotFoundException();
        }

        return byId.get();
    }
}
